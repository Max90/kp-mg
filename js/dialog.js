var persons = [];
var questionsArray = [];
var personIndex = 0;
var questionIndex = 0;
var questions;

$(function () {
    $.getJSON('assets/json/questions.json', function (data) {
        questions = data;
        buildArraysForAutoDialog(questions);
        $('#auto-dialog').click(function () {
            startAutoDialog(questions);
        });
        $('.person-button').click(function (event) {
            displayQuestionChooserDialog(questions, event.target.id);
        });
    });
});

function displayQuestionChooserDialog(questions) {
    $('.person-button').click(function () {
        var questionChooserContainer = $('#question-chooser-list');
        questionChooserContainer.text("");
        $('#answer-box').text("");
        var personName = this.id;
        var textToInsert = '';
        var possibleQuestions = questions[personName]['questions'];
        for (var i = 0; i < Object.keys(possibleQuestions).length; i++) {
            if (i % 2 === 0) {
                textToInsert += '<li id="question-' + personName + '-' + i + '" class="question-chooser-question even-question">' + possibleQuestions[i] + '</li>';
            } else {
                textToInsert += '<li id="question-' + personName + '-' + i + '" class="question-chooser-question odd-question">' + possibleQuestions[i] + '</li>';
            }
        }
        questionChooserContainer.append(textToInsert);
        displayAnswerForQuestion(questions);
    });
}

function displayAnswerForQuestion(questions) {
    $('.question-chooser-question').click(function () {
        var name = this.id.split('-')[1];
        var number = this.id.split('-')[2];
        var answer = questions[name]['answers'][number];
        $('#answer-box').text(answer);
        console.log(answer);
    });
}

function buildArraysForAutoDialog(questions) {
    $.each(questions, function (index, value) {
        persons.push(index);
    });
    return questionsArray;
}
function startAutoDialog() {
    if (questionIndex === questionsArray.length || questionsArray.length === 0) {
        if (questionsArray.length !== 0)
            personIndex++;
        questionIndex = 0;
        questionsArray = [];
        $.each(questions[persons[personIndex]]['questions'], function (index, value) {
            questionsArray.push(value);
        });
    }
    loadNextQuestionAnswerPairForAutoDialog(questions[persons[personIndex]], questionsArray[questionIndex], questions[persons[personIndex]]['answers'][questionIndex]);
}

function loadNextQuestionAnswerPairForAutoDialog(name, question, answer) {
    $('#question-chooser-list').html('<li id="question-' + name + '" class="question-chooser-question">' + question + '</li>');
    $('#answer-box').text(answer);
    questionIndex++;
    setTimeout(startAutoDialog, 1000);
}